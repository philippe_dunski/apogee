# Premier jour

Voici les objectifs pour le premier jour de développement du projet

- [v] Mise en place du dépot git (sur [framagit.org](https://framagit.org/philippe_dunski/apogee))
- [v] Ajout des fichiers "classiques", [README](../README.md) et [licence](../LICENSE.md) de base pour un projet git
- [v] Récupération des sources de [LLVM](https://github.com/llvm/llvm-project.git) et sélection de la dernière version
      stable