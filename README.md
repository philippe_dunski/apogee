# APOGEE : Another PersOnal Game Engine for Experiment

Apogee est l'acronyme de 
Another PersOnal Game Engine for Experiment, ce qui signifie,
pour les personne ne comprennant pas forcément bien la langue de
Shakespeare, 

> un autre moteur de jeu pour l'expérimentation

En outre, le terme apogée en lui-même se défini comme "le point culminant"
de quelque chose.

Au final, tant l'acronyme que le terme en lui-même donnent une explication
relativement précise des buts et des objectifs de ce projet:

Il s'agira de développer un (énième) moteur de jeux qui n'atteindra sans doute jamais
le niveau des moteurs de jeux les plus connus -- comme [ -->Unity<-- ](https://unity.com/), 
[ -->Unreal Engine<-- ](https://www.unrealengine.com) ou même 
[ -->Godot<-- ](https://godotengine.org/) -- (quoi que ...) et dont le développement aura
pour principal objectif de me permettre d'expériementer les techniques les plus récentes
et les plus avancées de mon langage de programmation de prédilection: le C++

## Faites ce que je dis, pas ce que je fais

Les gens qui ne connaissent bien savent que je suis généralement du genre à éviter autant
que possible se syndrome du NIH (Not Implemented Here), qui, en gros, incite les développeurs
à "réinventer" continuellement la roue, en développant par eux même des fonctionnalités
que d'autres ont déjà développées en y apportant une qualité souvent bien meilleure que
ce que l'on arrivera sans doute à obtenir dans un premier temps.

Cependant, ce projet est avant tout expérimental. Et, pour pouvoir expériementer au mieux
et se donner une chance d'apprendre de ses erreurs, rien ne vaut le fait de ... faire le maximum 
par soi-même.

Si bien qu'il ne sert à rien de le cacher, ce projet va -- souvent -- souffrir du NIH.

Par contre, j'essayerai, dans la mesure du possible, de fournir également une implémentation basée
sur des bibliothèques dont la qualité est avérée; ce qui me donnera une bonne idée du but à atteindre.